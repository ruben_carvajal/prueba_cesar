import ForgotPassword from './ForgotPassword'
import Login from './Login'

export {
	ForgotPassword,
	Login
}