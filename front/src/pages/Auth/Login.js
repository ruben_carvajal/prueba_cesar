import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    Button,
    Form,
} from 'reactstrap'
import { Field, reduxForm } from 'redux-form'
import { NavLink } from 'react-router-dom'

import Layout from './Layout'
import { Input } from '../../components/Inputs'
import {
    afterAuthenticated,
    authenticate,
    setAuthenticated,
    setAuthFeedback,
    setAuthToken,
    setAuthUser
} from '../../actions/Auth'
import {
    setErrorUI,
    setLoadingUI,
} from '../../actions/Session'
import { app as appConfig } from '../../config'
import { isEmpty } from '../../utils/helpers'
import messages from '../../utils/errors'

class Login extends Component {

    componentWillUnmount() {
        const { dispatch } = this.props
        dispatch(setAuthFeedback())
    }

    onSubmit = (values) => {
        const { dispatch } = this.props
        console.log('values', values)
        return authenticate(values).then(({ userInfo, token }) => {
            if (!userInfo || !token)
                dispatch(setAuthFeedback('Ha ocurrido un error', true))
            else {
                dispatch(setAuthFeedback())
                setAuthToken(token)
                dispatch(setAuthUser(userInfo))
                dispatch(setLoadingUI(true))
                dispatch(setAuthenticated(true))
                dispatch(afterAuthenticated(userInfo)).then(() => {
                    dispatch(setLoadingUI(false))
                }).catch((error) =>
                    dispatch(setErrorUI(error))
                )
            }
        }).catch(({ message }) =>
            dispatch(setAuthFeedback(message, true))
        )
    }

    render() {
        const { feedback, handleSubmit, submitting } = this.props
        return (
            <Layout
                feedback={feedback}
                subtitle={`${appConfig.description} ${appConfig.name}`}
                title='Iniciar Sesión'
            >
                <Form onSubmit={handleSubmit(this.onSubmit)} >
                    <Field
                        component={Input}
                        disabled={submitting}
                        label='Correo'
                        name='email'
                        required
                    />
                    <Field
                        component={Input}
                        disabled={submitting}
                        label='Contraseña'
                        name='password'
                        required
                        type='password'
                    />
                    <div className='d-flex flex-wrap align-items-center justify-content-between'>
                        <NavLink
                            to='/forgot'
                        >
                            ¿Olvidó su contraseña?
                        </NavLink>
                        <Button
                            color='primary'
                            disabled={submitting}
                            outline
                            type='submit'
                        >
                            {submitting ? 'Validando...' : 'Iniciar sesión'}
                        </Button>
                    </div>
                </Form>
            </Layout>
        )
    }
}

export default connect(({ auth }) => ({
    feedback: {
        error: auth.error,
        message: auth.message
    }
}))(reduxForm({
    form: 'login-form',
    validate: values => {
        const errors = {}
        if (isEmpty(values.email))
            errors.email = messages.required
        if (isEmpty(values.password))
            errors.password = messages.required
        return errors
    }
})(Login))