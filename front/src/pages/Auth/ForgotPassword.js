import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
    Button,
    Form,
} from 'reactstrap'
import { Field, reduxForm } from 'redux-form'
import { NavLink } from 'react-router-dom'

import Layout from './Layout'
import { Input } from '../../components/Inputs'
/*import {
    // forgotPassword,
    setAuthFeedback,
} from '../../actions/Auth'*/
import { isEmpty } from '../../utils/helpers'
import messages from '../../utils/errors'

class ForgotPassword extends Component {

    componentWillUnmount() {
        const { dispatch } = this.props
        //dispatch(setAuthFeedback())
    }

    onSubmit = (values) => {
        const { dispatch, history: { push } } = this.props
        /*return forgotPassword(values).then(({ message }) => {
            dispatch(setAuthFeedback(message))
            push('/login')
        }).catch(({ message }) =>
            dispatch(setAuthFeedback(message, true))
        )*/
    }

    render() {
        const { feedback, handleSubmit, submitting } = this.props
        return (
            <Layout
                feedback={feedback}
                subtitle='Ingrese su usuario y le enviaremos las indicaciones a su correo'
                title='Olvidó Contraseña'
            >
                <Form onSubmit={handleSubmit(this.onSubmit)} >
                    <Field
                        component={Input}
                        disabled={submitting}
                        label='Usuario'
                        name='usuario'
                        required
                    />
                    <div className='d-flex align-items-center justify-content-between'>
                        <NavLink
                            to='/login'
                        >
                            <i className='fas fa-arrow-left fa-fw mr-1'></i>Iniciar Sesión
                        </NavLink>
                        <Button
                            color='primary'
                            disabled={submitting}
                            outline
                            type='submit'
                        >
                            {submitting ? 'Enviando...' : 'Enviar'}
                        </Button>
                    </div>
                </Form>
            </Layout>
        )
    }
}

export default connect(({ auth }) => ({
    feedback: {
        error: auth.error,
        message: auth.message
    }
}))(reduxForm({
    form: 'forgot-password-form',
    validate: values => {
        const errors = {}
        if (isEmpty(values.usuario))
            errors.usuario = messages.required
        return errors
    }
})(ForgotPassword))