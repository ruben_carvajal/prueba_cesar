import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import {
    Col,
    Container,
    Row,
} from 'reactstrap'

import Alert from '../../../components/Alert'
import { app as appConfig } from '../../../config'
import './styles.css'

const FooterContent = () => (
    <div className='text-center small py-2'>
        <div className='text-muted'>
            {appConfig.name}
        </div>
        <a
            className='text-muted'
            href={`http://${appConfig.author.url}`}
            rel='noopener noreferrer'
            target='_blank'
        >
            &copy; {appConfig.author.name} {appConfig.year} - Todos los derechos reservados
        </a>
    </div>
)

const LayoutFooter = () => (
    <Fragment>
        <div
            className='d-none d-md-block position-absolute w-100'
            style={{ bottom: 0 }}
        >
            <FooterContent />
        </div>
        <div className='d-md-none mt-3'>
            <FooterContent />
        </div>
    </Fragment>
)

const Layout = ({
    children,
    feedback,
    subtitle,
    title,
}) => (
        <Container className='bg-white' fluid>
            <Row>
                <Col
                    className='full-height d-none d-md-flex flex-column align-items-center justify-content-center bg-gradient text-white'
                    md={6}
                    xl={7}
                >
                    <h2>{appConfig.name}</h2>
                    <p>{appConfig.slogan}</p>
                    <img
                        alt={appConfig.name}
                        className='img-fluid'
                        src='/images/font5.png'
                        style={{ width: "92%",
                            height: "auto" }}
                    />
                </Col>
                <Col
                    className='full-height px-md-0 d-md-flex align-items-center justify-content-center'
                    md={6}
                    style={{ overflow: 'hidden' }}
                    xl={5}
                >
                    <Col
                        md={{ offset: 0 }}
                        sm={{ size: 10, offset: 1 }}
                        xl={8}
                    >
                        <img
                            alt={appConfig.name}
                            className='d-block mx-auto img-fluid text-center my-3'
                            src='/images/logo-short-color.png'
                            style={{ maxWidth: 100 }}
                        />
                        {title && <h2 className='text-center'>{title}</h2>}
                        {subtitle && <p className='text-center text-muted'>{subtitle}</p>}
                        {feedback && feedback.message &&
                            <Alert
                                color={feedback.error ? 'danger' : 'success'}
                                message={feedback.message}
                                title={feedback.error ? '¡Lo sentimos!' : '¡Éxito!'}
                            />
                        }
                        <div className='pb-3'>
                            {children}
                        </div>
                    </Col>
                    <LayoutFooter />
                </Col>
            </Row>
        </Container>
    )

Layout.propTypes = {
    children: PropTypes.any.isRequired,
    feedback: PropTypes.shape({
        error: PropTypes.bool,
        message: PropTypes.string
    }),
    subtitle: PropTypes.string,
    title: PropTypes.string
}

export default Layout