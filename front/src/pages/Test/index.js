import React, { Fragment } from 'react'
import { Button } from 'reactstrap'
import { toastr } from 'react-redux-toastr'

import Alert from '../../components/Alert'
import { Area } from '../../components/Charts'
import Loader, { Bounce } from '../../components/Loader'
import Pagination, { SimplePagination } from '../../components/Pagination'
import Table from '../../components/Table'
import Form from './Form'

const Test = () => (
    <Fragment>
        <div className='d-flex flex-wrap justify-content-center mb-3'>
            <Button
                className='mr-3'
                color='success'
                onClick={() => toastr.success('Éxito', 'Mensaje éxito')}
            >
                Toastr Success
                </Button>
            <Button
                className='mr-3'
                color='danger'
                onClick={() => toastr.error('Error', 'Mensaje error')}
            >
                Toastr Danger
                </Button>
            <Button
                className='mr-3'
                color='info'
                onClick={() => toastr.info('Información', 'Mensaje información')}
            >
                Toastr Info
                </Button>
            <Button
                className='mr-3'
                color='warning'
                onClick={() => toastr.warning('Advertencia', 'Mensaje advertencia')}
            >
                Toastr Warning
            </Button>
            <Button
                className='mr-3'
                onClick={() => toastr.confirm('¿Desea confirmar?')}
            >
                Toastr Confirm
            </Button>
        </div>
        <div className='d-flex justify-content-around mb-3'>
            <div>
                <div className='mr-2'>Default Loader</div>
                <Loader />
            </div>
            <div>
                <div className='mr-2'>Bounce Loader</div>
                <Bounce size={25} />
            </div>
        </div>
        <div className='d-flex justify-content-center mb-3'>
            <Pagination
                activePage={6}
                items={12}
                next
                onSelect={(page) => null}
                previous
            />
        </div>
        <div className='mb-3'>
            <SimplePagination
                activePage={2}
                perPage={5}
                total={12}
                next
                onSelect={(page) => null}
                previous
            />
        </div>
        <div className='mb-3'>
            <Alert />
        </div>
        <div className='mb-3'>
            <Area
                data={[
                    {
                        nombre: 'A',
                        valor: 12
                    },
                    {
                        nombre: 'AB',
                        valor: 1233
                    }
                ]}
                xDataKey='nombre'
                yDataKey='valor'
            />
        </div>
        <div className='mb-3'>
            <Table
                columns={[
                    {
                        dataField: 'id',
                        text: 'ID'
                    },
                    {
                        dataField: 'nombre',
                        sort: true,
                        text: 'Producto',
                    },
                ]}
                data={[
                    {
                        id: 1,
                        nombre: 'Prueba'
                    },
                    {
                        id: 2,
                        nombre: 'Prueba1'
                    },
                    {
                        id: 3,
                        nombre: 'Prueba2'
                    }
                ]}
                keyField='id'
            />
        </div>
        <div className='mb-3'>
            <Form />
        </div>
    </Fragment>
)

export default Test