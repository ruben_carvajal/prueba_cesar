import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { Button, Form, FormGroup } from 'reactstrap'

import {
    AsyncTypeahead,
    CreatableSelect,
    DatePicker,
    DateRangePicker,
    Input,
    InputGroup,
    Select,
    Typeahead
} from '../../components/Inputs'
import messages from '../../utils/errors'
import { isEmail, isEmpty } from '../../utils/helpers'

const selectOptions = [
    { value: 'chocolate', lbl: 'Chocolate' },
    { value: 'strawberry', lbl: 'Strawberry', isDisabled: true },
    { value: 'vanilla', lbl: 'Vanilla' }
]

const typeaheadOptions = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' }
]

const FormTest = ({ handleSubmit }) => (
    <Form onSubmit={handleSubmit(() => null)} >
        <Field
            component={Input}
            label='Label Input'
            maxLength={12}
            name='campo1'
            placeholder='Placeholder input'
            type='text'
            required
        />
        <Field
            component={InputGroup}
            icon='far fa-envelope-open fa-fw'
            label='Label InputGroup'
            name='campo2'
            placeholder='Placeholder inputgroup'
            type='text'
            required
        />
        <Field
            component={Input}
            label='Label Email Input'
            name='campo3'
            placeholder='Placeholder email input'
            type='email'
            required
        />
        <Field
            component={DatePicker}
            label='Label Datepicker'
            name='campo4'
            placeholder='Placeholder datepicker'
            required
        />
        <Field
            component={DateRangePicker}
            label='Label Daterangepicker'
            name='campo5'
            placeholder='Placeholder daterangepicker'
            required
        />
        <Field
            component={Select}
            errorLoading
            getOptionLabel={(option) => option.lbl}
            label='Label Select'
            name='campo6'
            options={selectOptions}
            placeholder='Placeholder select'
            required
        />
        <Field
            component={CreatableSelect}
            getOptionLabel={(option) => option.lbl}
            label='Label Creatable Select'
            name='campo7'
            options={selectOptions}
            placeholder='Placeholder creatable select'
            required
        />
        <Field
            allowNew
            component={AsyncTypeahead}
            label='Label AsyncTypeahead'
            minLength={0}
            name='campo8'
            onSearch={() => null}
            options={typeaheadOptions}
            required
        />
        <Field
            component={Typeahead}
            label='Label Typeahead'
            name='campo9'
            minLength={0}
            options={typeaheadOptions}
            required
        />
        <FormGroup>
            <Button type='submit'>Submit</Button>
        </FormGroup>
    </Form>
)

export default reduxForm({
    form: 'simple-form-fields',
    initialValues: {
        campo5: DateRangePicker.yesterday,
    },
    validate: values => {
        const errors = {}
        if (isEmpty(values.campo1))
            errors.campo1 = messages.required
        if (!isEmail(values.campo2))
            errors.campo2 = messages.email
        if (isEmpty(values.campo3))
            errors.campo3 = messages.required
        if (isEmpty(values.campo4))
            errors.campo4 = messages.required
        if (isEmpty(values.campo5))
            errors.campo5 = messages.required
        if (isEmpty(values.campo6))
            errors.campo6 = messages.required
        if (isEmpty(values.campo7))
            errors.campo7 = messages.required
        if (isEmpty(values.campo8))
            errors.campo8 = messages.required
        if (isEmpty(values.campo9))
            errors.campo9 = messages.required
        return errors
    }
})(FormTest)