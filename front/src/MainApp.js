import React from 'react'
import AppRoutes from './AppRoutes'

import 'bootstrap-daterangepicker/daterangepicker.css'
import 'react-datetime/css/react-datetime.css'
//import 'react-bootstrap-typeahead/css/Typeahead.css' // v4 broken Typeahead-bs4.css
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'

import ReduxToastr from 'react-redux-toastr'

const MainApp = () => (
    [
        <AppRoutes
            key='app-routes'
        />,
        <ReduxToastr
            closeOnToastrClick
            confirmOptions={{
                cancelText: 'Cancelar',
                okText: 'Aceptar',
            }}
            key='app-tostr'
            newestOnTop={false}
            preventDuplicates
            progressBar
            position='top-right'    // top-left, top-center, top-right, bottom-left, bottom-center, bottom-right
            timeOut={4000}
            transitionIn='fadeIn'   //bounceIn, bounceInDown, fadeIn
            transitionOut='fadeOut' //bounceOut, bounceOutUp, fadeOut
        />
    ]
)

export default MainApp