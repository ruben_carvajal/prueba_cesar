export default [
    {
        name: 'Administración',
        url: '/administracion',
        icon: 'fas fa-cubes fa-fw'
    },
    {
        name: 'Cotizaciones',
        url: '/cotizaciones',
        icon: 'fas fa-plus fa-fw'
    },
    {
        name: 'Logistica',
        url: '/logistica',
        icon: 'fas fa-tags fa-fw'
    },
    {
        name: 'Reporte',
        url: '/reporte',
        icon: 'fas fa-tags fa-fw'
    },
    {
        name: 'Test',
        url: '/test',
        icon: 'fas fa-th-list fa-fw'
    },
]