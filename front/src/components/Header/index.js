import React, { Fragment, Component } from 'react'
import { toastr } from 'react-redux-toastr'
import { NavLink, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import {
    Nav,
    DropdownItem,
    DropdownMenu,
    DropdownToggle
} from 'reactstrap'
import {
    AppNavbarBrand,
    AppSidebarToggler,
    AppHeaderDropdown
} from '@coreui/react'
import {
    logout,
    doLogout,
    setAuthFeedback,
} from '../../actions/Auth'

import { app as appConfig } from '../../config'

class Header extends Component {

    constructor(props) {
        super(props)
        this.state = {
            feedback: false,
        }
    }

    componentDidMount() {
        const { user } = this.props

        if (user) {
            console.log('Usuario conectado')
        }
    }

    render() {
        const {
            dispatch,
        } = this.props

        return (
            <Fragment>
                <AppSidebarToggler className='d-lg-none' display='md' mobile />
                <AppNavbarBrand tag={NavLink} to='/'>
                    {appConfig.name}
                </AppNavbarBrand>
                <AppSidebarToggler className='d-md-down-none' display='lg' />
                <Nav className='ml-auto' navbar>
                    <AppHeaderDropdown direction='down'>
                        <DropdownToggle caret nav>
                            <i className='far fa-user fa-fw'></i>
                        </DropdownToggle>
                        <DropdownMenu right style={{ left: 'auto' }}>
                            <DropdownItem>
                                <i className='fas fa-user fa-fw'></i>Perfil
                            </DropdownItem>
                            <DropdownItem onClick={() => {
                                        logout().then(() => {
                                            dispatch(setAuthFeedback('Ha cerrado sesión'))
                                            dispatch(doLogout())
                                        }).catch(({ message }) =>
                                            toastr.error('Error', message)
                                        )
                                    }}>
                                <i className='fas fa-sign-out-alt fa-fw'></i> Cerrar Sesión
                            </DropdownItem>
                        </DropdownMenu>
                    </AppHeaderDropdown>
                </Nav>
            </Fragment>
        )
    }



}

export default withRouter(connect(({ auth, session }) => ({
    user: auth.user,
}))(Header))