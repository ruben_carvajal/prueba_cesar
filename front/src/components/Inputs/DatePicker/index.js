import React from 'react'
import PropTypes from 'prop-types'
import { Input } from 'reactstrap'
import DateTimePicker from 'react-datetime'
import moment from 'moment'

import CommonInput from '../CommonInput'
import { parseFieldProps } from '../utils'
import './styles.css'

const CustomDatePicker = ({
    className,
    closeOnSelect,
    closeOnTab,
    containerClassName,
    dateFormat,
    datePickerInput,
    disableCloseOnClickOutside,
    isValidDate,
    label,
    locale,
    onBlur,
    onChange,
    onFocus,
    onNavigateBack,
    onNavigateForward,
    onViewModeChange,
    open,
    placeholder,
    renderDay,
    renderMonth,
    renderYear,
    required,
    timeConstraints,
    timeFormat,
    utc,
    value,
    viewDate,
    viewMode,
    ...props
}) => {
    const { error, onBlur: inputBlur, onChange: inputChange, onFocus: inputFocus, value: inputValue } = parseFieldProps(props)

    return (
        <CommonInput
            className={containerClassName}
            label={label}
            required={required}
            {...props}
        >
            <DateTimePicker
                className={className}
                closeOnSelect={closeOnSelect}
                closeOnTab={closeOnTab}
                dateFormat={dateFormat}
                disableCloseOnClickOutside={disableCloseOnClickOutside}
                input={datePickerInput}
                inputProps={{
                    placeholder,
                    readOnly: true,
                }}
                isValidDate={isValidDate}
                locale={locale}
                onBlur={inputBlur ? () => inputBlur(inputValue) : onBlur}
                onChange={inputChange || onChange}
                onFocus={inputFocus || onFocus}
                onNavigateBack={onNavigateBack}
                onNavigateForward={onNavigateForward}
                onViewModeChange={onViewModeChange}
                open={open}
                renderDay={renderDay}
                renderInput={(innerProps, openCalendar, closeCalendar) => (
                    <Input
                        {...innerProps}
                        invalid={error}
                    />
                )}
                renderMonth={renderMonth}
                renderYear={renderYear}
                timeConstraints={timeConstraints}
                timeFormat={timeFormat}
                utc={utc}
                viewDate={viewDate}
                viewMode={viewMode}
                value={inputValue || value}
            />
        </CommonInput>
    )
}

CustomDatePicker.propTypes = {
    className: PropTypes.string,
    closeOnSelect: PropTypes.bool,
    closeOnTab: PropTypes.bool,
    containerClassName: PropTypes.string,
    dateFormat: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.string,
    ]),
    disableCloseOnClickOutside: PropTypes.bool,
    datePickerInput: PropTypes.bool, //Custom input -> input prop 
    isValidDate: PropTypes.func,
    locale: PropTypes.string,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onNavigateBack: PropTypes.func,
    onNavigateForward: PropTypes.func,
    onViewModeChange: PropTypes.func,
    open: PropTypes.bool,
    placeholder: PropTypes.string,
    renderDay: PropTypes.func,
    renderMonth: PropTypes.func,
    renderYear: PropTypes.func,
    timeConstraints: PropTypes.object,
    timeFormat: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.string,
    ]),
    utc: PropTypes.bool,
    viewDate: PropTypes.instanceOf(Date),
    value: PropTypes.oneOfType([
        PropTypes.instanceOf(Date),
        PropTypes.instanceOf(moment),

    ]),
    viewMode: PropTypes.oneOf([
        'days',
        'months',
        'time',
        'years'
    ]),
}

CustomDatePicker.defaultProps = {
    closeOnSelect: true, //default false
    closeOnTab: true, //default true
    dateFormat: 'DD/MM/YYYY', //default true
    disableCloseOnClickOutside: false, //default false
    datePickerInput: true, //Custom input -> input prop -> default true
    placeholder: 'Seleccione...',
    timeFormat: false,
    utc: false, //default false
    viewMode: 'days', //default 'days'
}

export default CustomDatePicker