import React from 'react'
import PropTypes from 'prop-types'
import { FormGroup } from 'reactstrap'

import InputLabel from '../Label'
import {
    fieldProptypes,
    inputProptypes
} from '../proptypes'
import { parseFieldProps } from '../utils'

const CommonInput = ({
    children,
    className,
    label,
    labelClassName,
    meta,
    required,
}) => {
    const { error, feedback } = parseFieldProps({ meta })

    return (
        <FormGroup className={className}>
            {typeof label !== "string" ? label :
                (label &&
                    <InputLabel
                        className={labelClassName}
                        label={label}
                        required={required}
                    />)}
            {children}
            {error && <div className="text-danger mt-1 small">
                {feedback}
            </div>}
        </FormGroup>
    )
}

CommonInput.propTypes = {
    ...fieldProptypes,
    ...inputProptypes,
    className: PropTypes.string,
    labelClassName: PropTypes.string,
    children: PropTypes.any.isRequired,
}

export default CommonInput