export const parseFieldProps = ({ input, meta }) => {

    const inputOnBlur = input ? input.onBlur : undefined
    const inputOnChange = input ? input.onChange : undefined
    const inputOnDragStart = input ? input.onDragStart : undefined
    const inputOnDrop = input ? input.onDrop : undefined
    const inputOnFocus = input ? input.onFocus : undefined
    const inputValue = input ? input.value : undefined
    const inputType = input ? input.type : undefined

    const invalid = meta ? meta.invalid : false
    const feedBackMessage = meta ? meta.error : ''
    const touched = meta ? meta.touched : false

    return {
        error: touched && invalid,
        feedback: feedBackMessage,
        invalid,
        onBlur: inputOnBlur,
        onChange: inputOnChange,
        onDragStart: inputOnDragStart,
        onDrop: inputOnDrop,
        onFocus: inputOnFocus,
        touched,
        value: inputValue,
        type: inputType
    }
}