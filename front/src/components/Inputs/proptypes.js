import PropTypes from 'prop-types'

/* 
    Field  Proptypes- Redux Form
 */
const fieldProptypes = {
    input: PropTypes.shape({
        checked: PropTypes.bool,
        name: PropTypes.string,
        onBlur: PropTypes.func,
        onChange: PropTypes.func,
        onDragStart: PropTypes.func,
        onDrop: PropTypes.func,
        onFocus: PropTypes.func,
        value: PropTypes.any
    }),
    meta: PropTypes.shape({
        active: PropTypes.bool,
        autofilled: PropTypes.bool,
        asyncValidating: PropTypes.bool,
        dirty: PropTypes.bool,
        dispatch: PropTypes.func,
        error: PropTypes.string,
        form: PropTypes.string,
        initial: PropTypes.any,
        invalid: PropTypes.bool,
        pristine: PropTypes.bool,
        submitting: PropTypes.bool,
        submitFailed: PropTypes.bool,
        touched: PropTypes.bool,
        valid: PropTypes.bool,
        visited: PropTypes.bool,
        warning: PropTypes.string,
    }),
}

/*  
    Common proptypes of an input 
*/
const inputProptypes = {
    label: PropTypes.any,
    placeholder: PropTypes.string,
    required: PropTypes.bool,
}

export {
    fieldProptypes,
    inputProptypes
}