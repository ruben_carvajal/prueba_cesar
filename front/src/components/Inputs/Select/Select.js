import React from 'react'
import Select from 'react-select'

import CommonInput from '../CommonInput'
import { parseFieldProps } from '../utils'

import { defaultProps } from './defaults'
import { getStyles, getOptions, getValue, parseOptionLabel } from './utils'
import proptypes from './proptypes'

const CustomSelect = ({
    autoFocus,
    backspaceRemovesValue,
    blurInputOnSelect,
    captureMenuScroll,
    className,
    containerClassName,
    delimiter,
    disabled,
    escapeClearsValue,
    errorLoading,
    getOptionLabel,
    getOptionValue,
    hideSelectedOptions,
    id,
    inputId,
    inputValue,
    isClearable,
    isDisabled,
    isLoading,
    isMulti,
    isRtl,
    isSearchable,
    label,
    loadingMessage,
    minMenuHeight,
    maxMenuHeight,
    menuIsOpen,
    menuPlacement,
    menuPortalTarget,
    menuPosition,
    noOptionsMessage,
    onBlur,
    onChange,
    onFocus,
    onInputChange,
    onKeyDown,
    onMenuClose,
    onMenuOpen,
    options,
    pageSize,
    placeholder,
    required,
    value,
    ...props
}) => {
    const { error, onBlur: inputBlur, onChange: inputChange, value: inputVal } = parseFieldProps(props)

    return (
        <CommonInput
            className={containerClassName}
            label={label}
            required={required}
            {...props}
        >
            <Select
                autoFocus={autoFocus}
                backspaceRemovesValue={backspaceRemovesValue}
                blurInputOnSelect={blurInputOnSelect}
                captureMenuScroll={captureMenuScroll}
                className={className}
                delimiter={delimiter}
                escapeClearsValue={escapeClearsValue}
                getOptionLabel={parseOptionLabel({ getOptionLabel, errorLoading })}
                getOptionValue={getOptionValue}
                hideSelectedOptions={hideSelectedOptions}
                id={id}
                inputId={inputId}
                inputValue={inputValue}
                isClearable={isClearable}
                isDisabled={disabled || isDisabled || isLoading}
                isLoading={isLoading}
                isMulti={isMulti}
                isRtl={isRtl}
                isSearchable={isSearchable}
                loadingMessage={loadingMessage}
                minMenuHeight={minMenuHeight}
                maxMenuHeight={maxMenuHeight}
                menuIsOpen={menuIsOpen}
                menuPlacement={menuPlacement}
                menuPortalTarget={menuPortalTarget}
                menuPosition={menuPosition}
                noOptionsMessage={noOptionsMessage}
                onBlur={inputBlur ? () => inputBlur(getValue({ errorLoading, inputVal, value })) : onBlur}
                onChange={inputChange || onChange}
                onFocus={onFocus}
                onInputChange={onInputChange}
                onKeyDown={onKeyDown}
                onMenuClose={onMenuClose}
                onMenuOpen={onMenuOpen}
                options={getOptions({ errorLoading, options })}
                pageSize={pageSize}
                placeholder={isLoading ? 'Cargando...' : placeholder}
                styles={getStyles({ error })}
                value={getValue({ errorLoading, inputVal, value })}
            />
        </CommonInput>
    )
}

CustomSelect.propTypes = proptypes

CustomSelect.defaultProps = defaultProps

export default CustomSelect