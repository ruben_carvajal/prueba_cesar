/* 
    Common Select default props  
*/
export const defaultProps = {
    backspaceRemovesValue: false, //default true
    blurInputOnSelect: false,
    escapeClearsValue: false,//default false
    errorLoading: false, // Custom prop
    isClearable: false,
    isLoading: false, //default false
    isMulti: false, //default false
    isRtl: false, //default false
    isSearchable: false, //default true
    loadingMessage: ({
        inputValue
    }) => (`${inputValue ? `Buscando coincidencias para: ${inputValue} ...` : 'Cargando espere...'}`),
    minMenuHeight: 140, // default 140
    maxMenuHeight: 300, //default 300
    menuPlacement: 'bottom', //default bottom
    menuPosition: 'absolute', //default absolute
    noOptionsMessage: ({
        inputValue
    }) => (`${inputValue ? `No hay resultados para: ${inputValue}` : 'No hay elementos para mostrar'}`),
    pageSize: 5, //default 5
    placeholder: 'Seleccione...',
    value: null,
}