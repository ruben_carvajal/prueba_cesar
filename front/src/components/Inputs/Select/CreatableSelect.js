import React from 'react'
import PropTypes from 'prop-types'
import Creatable from 'react-select/creatable';

import CommonInput from '../CommonInput'
import { parseFieldProps } from '../utils'

import { defaultProps } from './defaults'
import { getStyles, getOptions, getValue, parseOptionLabel } from './utils'
import proptypes from './proptypes'

const CreatableSelect = ({
    allowCreateWhileLoading,
    containerClassName,
    createOptionPosition,
    errorLoading,
    formatCreateLabel,
    getOptionLabel,
    isValidNewOption,
    getNewOptionData,
    label,
    onBlur,
    onChange,
    onCreateOption,
    options,
    required,
    value,
    ...props
}) => {
    const { error, onBlur: inputBlur, onChange: inputChange, value: inputVal } = parseFieldProps(props)
    return (
        <CommonInput
            className={containerClassName}
            label={label}
            required={required}
            {...props}
        >
            <Creatable
                allowCreateWhileLoading={allowCreateWhileLoading}
                createOptionPosition={createOptionPosition}
                formatCreateLabel={formatCreateLabel}
                getOptionLabel={parseOptionLabel({ getOptionLabel, errorLoading })}
                isValidNewOption={isValidNewOption}
                getNewOptionData={getNewOptionData}
                onBlur={inputBlur ? () => inputBlur(getValue({ errorLoading, inputVal, value })) : onBlur}
                onChange={inputChange || onChange}
                onCreateOption={onCreateOption}
                options={getOptions({ errorLoading, options })}
                styles={getStyles({ error })}
                value={getValue({ errorLoading, inputVal, value })}
                {...props}
            />
        </CommonInput>
    )
}

CreatableSelect.propTypes = {
    ...proptypes,
    allowCreateWhileLoading: PropTypes.bool,
    createOptionPosition: PropTypes.oneOf(['first', 'last']),
    formatCreateLabel: PropTypes.func,
    isValidNewOption: PropTypes.func,
    getNewOptionData: PropTypes.func,
    onCreateOption: PropTypes.func,
}

CreatableSelect.defaultProps = {
    ...defaultProps,
    allowCreateWhileLoading: false, //default false
    createOptionPosition: 'last', // default last
    formatCreateLabel: (inputValue) => (`Seleccionar: ${inputValue}`),
}

export default CreatableSelect