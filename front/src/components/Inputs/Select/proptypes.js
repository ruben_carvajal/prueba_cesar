import PropTypes from 'prop-types'

/* 
    Common Select proptypes 
*/
export default {
    autoFocus: PropTypes.bool,
    backspaceRemovesValue: PropTypes.bool,
    blurInputOnSelect: PropTypes.bool,
    captureMenuScroll: PropTypes.bool,
    className: PropTypes.string,
    containerClassName: PropTypes.string,
    delimiter: PropTypes.string,
    disabled: PropTypes.bool, //custom
    escapeClearsValue: PropTypes.bool,
    errorLoading: PropTypes.bool,
    getOptionLabel: PropTypes.func,
    getOptionValue: PropTypes.func,
    hideSelectedOptions: PropTypes.bool,
    id: PropTypes.string,
    inputId: PropTypes.string,
    inputValue: PropTypes.string,
    isClearable: PropTypes.bool,
    isDisabled: PropTypes.bool,
    isLoading: PropTypes.bool,
    isMulti: PropTypes.bool,
    isRtl: PropTypes.bool,
    isSearchable: PropTypes.bool,
    loadingMessage: PropTypes.func,
    minMenuHeight: PropTypes.number,
    menuIsOpen: PropTypes.bool,
    menuPlacement: PropTypes.oneOf(['auto', 'bottom', 'top']),
    menuPortalTarget: PropTypes.any,
    menuPosition: PropTypes.oneOf(['absolute', 'fixed']),
    noOptionsMessage: PropTypes.func,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    onFocus: PropTypes.func,
    onInputChange: PropTypes.func,
    onKeyDown: PropTypes.func,
    onMenuClose: PropTypes.func,
    onMenuOpen: PropTypes.func,
    options: PropTypes.array,
    pageSize: PropTypes.number,
    value: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.object,
    ]),
}