const DEFAULT_LABEL_KEY = 'label'

export const getStyles = ({ error }) => {
    return {
        control: (innerStyles) => {
            if (error)
                return { ...innerStyles, boxShadow: 'none', borderColor: 'red !important' }
            return innerStyles
        },
        clearIndicator: (innerStyles) => ({ ...innerStyles, color: 'var(--gray)' }),
        menuPortal: (innerStyles) => ({ ...innerStyles, zIndex: 9999 }),
        placeholder: (innerStyles) => ({ ...innerStyles, color: 'var(--gray)' }),
    }
}

export const getOptions = ({ errorLoading, options }) => {
    if (errorLoading)
        return [
            {
                [DEFAULT_LABEL_KEY]: '¡Lo sentimos! Ha ocurrido un error',
                isDisabled: true
            }
        ]
    return options
}

export const parseOptionLabel = ({ errorLoading, getOptionLabel }) => (
    errorLoading ?
        (option) => option[DEFAULT_LABEL_KEY] :
        getOptionLabel
)

export const getValue = ({ errorLoading, inputVal, value }) => {
    if (errorLoading)
        return null
    return inputVal || value
}