import React from 'react'
import PropTypes from 'prop-types'
import { Input } from 'reactstrap'
import CommonInput from '../CommonInput'

import { parseFieldProps } from '../utils'

const CustomInput = ({
    className,
    containerClassName,
    input,
    meta,
    required,
    ...props
}) => {

    const { error, invalid, touched, ...inputProps } = parseFieldProps({ meta, input })

    return (
        <CommonInput
            className={containerClassName}
            input={input}
            meta={meta}
            required={required}
            {...props}
        >
            <Input
                className={className}
                invalid={error}
                {...inputProps}
                {...props}
            />
        </CommonInput>
    )
}

CustomInput.propTypes = {
    className: PropTypes.string,
    containerClassName: PropTypes.string,
}

export default CustomInput