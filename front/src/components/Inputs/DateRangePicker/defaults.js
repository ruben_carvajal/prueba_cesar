import {
    today,
    tomorrow,
    yesterday,
    restOfWeek,
    thisWeek,
    thisMonth
} from './ranges'

export const ranges = {
    'Today': [today.startDate, today.endDate],
    'Tomorrow': [tomorrow.startDate, tomorrow.endDate],
    'Yesterday': [yesterday.startDate, yesterday.endDate],
    'Rest of the week': [restOfWeek.startDate, restOfWeek.endDate],
    'This week': [thisWeek.startDate, thisWeek.endDate],
    'This month': [thisMonth.startDate, thisMonth.endDate],
}

export const dateFormat = 'DD/MM/YYYY'

export const rangeSeparator = ' - '

export const timeFormat = 'HH:mm'

export const timeSecondsFormat = `${timeFormat}:ss`

export const locale = {
    applyLabel: 'Aplicar',
    cancelLabel: 'Cancelar',
    customRangeLabel: 'Personalizado',
    daysOfWeek: [
        'Do',
        'Lu',
        'Ma',
        'Mi',
        'Ju',
        'Vi',
        'Sá'
    ],
    firstDay: 1,
    format: dateFormat,
    fromLabel: 'Desde',
    monthNames: [
        'Enero',
        'Febrero',
        'Marzo',
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre'
    ],
    separator: rangeSeparator,
    toLabel: 'Hasta',
    weekLabel: 'S'
}