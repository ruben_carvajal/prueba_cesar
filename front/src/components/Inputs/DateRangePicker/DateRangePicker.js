import React from 'react'
import PropTypes from 'prop-types'
import { Input } from 'reactstrap'
import DateRangePicker from 'react-bootstrap-daterangepicker'
import moment from 'moment'
import 'moment/locale/es'

import CommonInput from '../CommonInput'
import { parseFieldProps } from '../utils'
import {
    dateFormat,
    locale as defaultLocale,
    ranges as defaultRanges,
    rangeSeparator,
    timeFormat,
    timeSecondsFormat,
} from './defaults'
import './styles.css'

const calculateValue = ({
    timePicker,
    timePickerSeconds,
    value,
    ...rest
}) => {
    let renderValue = ''

    if (value && value.startDate && value.endDate) {
        const format = `${dateFormat}${timePicker ? ` ${timePickerSeconds ? timeSecondsFormat : timeFormat}` : ''}`;
        const { endDate, startDate } = value
        let end, start

        start = moment(startDate).format(format);
        end = moment(endDate).format(format);
        renderValue = start === end ? start : `${start}${rangeSeparator}${end}`;
    }
    return renderValue
}

const DateTimeRangePicker = ({
    alwaysShowCalendars,
    applyButtonClasses,
    autoApply,
    autoUpdateInput,
    buttonClasses,
    cancelButtonClasses,
    containerClassName,
    containerStyles,
    disabled,
    drops,
    isCustomDate,
    isInvalidDate,
    label,
    linkedCalendars,
    locale,
    maxDate,
    maxSpan,
    maxYear,
    minDate,
    minYear,
    onBlur,
    onChange,
    opens,
    parentEl,
    pickerContainerClassName,
    placeholder,
    ranges,
    required,
    showCustomRangeLabel,
    showDropdowns,
    showISOWeekNumbers,
    showWeekNumbers,
    singleDatePicker,
    timePicker,
    timePickerIncrement,
    timePickerSeconds,
    timePicker24Hour,
    value,
    ...props
}) => {
    const pickerLocale = timePicker ? Object.assign(locale, { format: `${dateFormat} ${timePickerSeconds ? timeSecondsFormat : timeFormat}` }) : locale
    const { error, onBlur: inputBlur, onChange: inputChange, value: inputValue } = parseFieldProps(props)
    const inptVal = inputValue || value || {}
    const renderValue = calculateValue({ ...props, value: inptVal })
    const inpChange = inputChange || onChange
    const { startDate, endDate } = inptVal

    return (
        <CommonInput
            className={containerClassName}
            label={label}
            required={required}
            {...props}
        >
            {disabled ?
                <Input
                    disabled={disabled}
                    invalid={error}
                    placeholder={placeholder}
                    onBlur={inputBlur ? () => inputBlur(inputValue) : onBlur}
                    readOnly={!disabled}
                    value={renderValue}
                /> :
                <DateRangePicker
                    alwaysShowCalendars={alwaysShowCalendars}
                    applyButtonClasses={applyButtonClasses}
                    autoApply={autoApply}
                    autoUpdateInput={autoUpdateInput}
                    buttonClasses={buttonClasses}
                    cancelButtonClasses={cancelButtonClasses}
                    containerClass={pickerContainerClassName}
                    containerStyles={containerStyles}
                    drops={drops}
                    endDate={endDate}
                    isCustomDate={isCustomDate}
                    isInvalidDate={isInvalidDate}
                    linkedCalendars={linkedCalendars}
                    locale={pickerLocale}
                    maxDate={maxDate}
                    maxSpan={maxSpan}
                    maxYear={maxYear}
                    minDate={minDate}
                    minYear={minYear}
                    onApply={(event, { endDate: selectedEndDate, startDate: selectedStartDate }) => {
                        if (!timePickerSeconds) {
                            selectedStartDate.startOf('minute');
                            selectedEndDate.startOf('minute')
                        }

                        if (timePicker && timePickerIncrement > 1) {
                            const startRemainder = selectedStartDate.minute() % timePickerIncrement;
                            if (startRemainder > 0)
                                selectedStartDate.add(timePickerIncrement - startRemainder, "minutes")
                            const endRemainder = selectedEndDate.minute() % timePickerIncrement;
                            if (endRemainder > 0)
                                selectedEndDate.add(timePickerIncrement - endRemainder, "minutes")
                        }
                        inpChange && inpChange({ startDate: selectedStartDate, endDate: selectedEndDate })
                    }}
                    opens={opens}
                    parentEl={parentEl}
                    ranges={singleDatePicker ? null : ranges}
                    showCustomRangeLabel={singleDatePicker ? false : showCustomRangeLabel}
                    showDropdowns={showDropdowns}
                    showISOWeekNumbers={showISOWeekNumbers}
                    showWeekNumbers={showWeekNumbers}
                    singleDatePicker={singleDatePicker}
                    startDate={startDate}
                    timePicker={timePicker}
                    timePickerIncrement={timePickerIncrement}
                    timePickerSeconds={timePickerSeconds}
                    timePicker24Hour={timePicker24Hour}
                >
                    <Input
                        disabled={disabled}
                        invalid={error}
                        placeholder={placeholder}
                        onBlur={inputBlur ? () => inputBlur(inputValue) : onBlur}
                        readOnly={!disabled}
                        value={renderValue}
                    />
                </DateRangePicker>
            }
        </CommonInput>
    )
}

DateTimeRangePicker.propTypes = {
    alwaysShowCalendars: PropTypes.bool,
    applyButtonClasses: PropTypes.string,
    autoApply: PropTypes.bool,
    autoUpdateInput: PropTypes.bool,
    buttonClasses: PropTypes.string,
    cancelButtonClasses: PropTypes.string,
    containerClassName: PropTypes.string,
    containerStyles: PropTypes.object,
    disabled: PropTypes.bool, //custom
    drops: PropTypes.oneOf(['down', 'up']),
    isCustomDate: PropTypes.func,
    isInvalidDate: PropTypes.func,
    linkedCalendars: PropTypes.bool,
    locale: PropTypes.shape({
        applyLabel: PropTypes.string,
        cancelLabel: PropTypes.string,
        customRangeLabel: PropTypes.string,
        daysOfWeek: PropTypes.arrayOf(PropTypes.string),
        firstDay: PropTypes.number,
        format: PropTypes.string,
        fromLabel: PropTypes.string,
        monthNames: PropTypes.arrayOf(PropTypes.string),
        separator: PropTypes.string,
        toLabel: PropTypes.string,
        weekLabel: PropTypes.string,
    }),
    maxDate: PropTypes.oneOfType([
        PropTypes.instanceOf(Date),
        PropTypes.instanceOf(moment),
        PropTypes.string,
    ]),
    maxSpan: PropTypes.oneOfType([
        PropTypes.shape({
            days: PropTypes.number,
        }),
        PropTypes.shape({
            months: PropTypes.number,
        }),
    ]),
    maxYear: PropTypes.number,
    minDate: PropTypes.oneOfType([
        PropTypes.instanceOf(Date),
        PropTypes.instanceOf(moment),
        PropTypes.string,
    ]),
    minYear: PropTypes.number,
    onBlur: PropTypes.func,
    onChange: PropTypes.func,
    opens: PropTypes.oneOf(['center', 'left', 'right']),
    parentEl: PropTypes.string,
    pickerContainerClassName: PropTypes.string,
    ranges: PropTypes.object,
    showCustomRangeLabel: PropTypes.bool,
    showDropdowns: PropTypes.bool,
    showISOWeekNumbers: PropTypes.bool,
    showWeekNumbers: PropTypes.bool,
    singleDatePicker: PropTypes.bool,
    timePicker: PropTypes.bool,
    timePickerIncrement: PropTypes.number, // In minutes
    timePickerSeconds: PropTypes.bool,
    timePicker24Hour: PropTypes.bool,
    value: PropTypes.shape({
        endDate: PropTypes.oneOfType([
            PropTypes.instanceOf(Date),
            PropTypes.instanceOf(moment),
            PropTypes.string,
        ]),
        startDate: PropTypes.oneOfType([
            PropTypes.instanceOf(Date),
            PropTypes.instanceOf(moment),
            PropTypes.string,
        ]),
    }),
}

DateTimeRangePicker.defaultProps = {
    alwaysShowCalendars: false,
    autoApply: true,
    autoUpdateInput: false,
    disabled: false,
    drops: 'down',
    linkedCalendars: false,
    locale: defaultLocale,
    opens: 'center',
    parentEl: 'body',
    pickerContainerClassName: 'd-block',
    placeholder: 'Seleccione...',
    ranges: defaultRanges,
    showCustomRangeLabel: true,
    showDropdowns: true,
    showWeekNumbers: false,
    singleDatePicker: false,
    timePicker: false,
    timePickerSeconds: false,
    timePicker24Hour: true,
}

export default DateTimeRangePicker