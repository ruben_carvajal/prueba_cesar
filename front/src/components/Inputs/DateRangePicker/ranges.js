import moment from 'moment'

export const today = {
    startDate: moment().startOf('day'),
    endDate: moment().endOf('day'),
}

export const tomorrow = {
    startDate: moment().add(1, 'days').startOf('day'),
    endDate: moment().add(1, 'days').endOf('day')
}

export const yesterday = {
    startDate: moment().subtract(1, 'days').startOf('day'),
    endDate: moment().subtract(1, 'days').endOf('day')
}

export const restOfWeek = {
    startDate: moment().startOf('day'),
    endDate: moment().endOf('week')
}

export const thisWeek = {
    startDate: moment().startOf('week'),
    endDate: moment().endOf('week')
}

export const last7Days = {
    startDate: moment().subtract(6, 'days').startOf('day'),
    endDate: moment().endOf('day')
}

export const lastWeek = {
    startDate: moment().subtract(1, 'weeks').startOf('week'),
    endDate: moment().subtract(1, 'weeks').endOf('week'),
}

export const thisMonth = {
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month')
}

export const lastMonth = {
    startDate: moment().subtract(1, 'months').startOf('month'),
    endDate: moment().subtract(1, 'months').endOf('month')
}

export const last30Days = {
    startDate: moment().subtract(29, 'days').startOf('day'),
    endDate: moment().endOf('day')
}