import DateRangePicker from './DateRangePicker'
import * as ranges from './ranges'

export default Object.assign(DateRangePicker, { ...ranges });