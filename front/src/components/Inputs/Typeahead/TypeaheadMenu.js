import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Highlighter, Menu, MenuItem } from 'react-bootstrap-typeahead'

import { defaultProps } from './defaults'
import proptypes from './proptypes'
import { getStringLabelKey, getOptionLabel } from './utils'

const MenuAsyncPaginationItem = ({
    asyncPaginate,
    disabled,
    label,
    onAsyncPaginate,
    position,
    text,
}) => (
        <Fragment>
            <Menu.Divider />
            <li
                aria-label={label}
                aria-selected='false'
                className={`rbt-menu-pagination-option ${disabled ? 'disabled' : ''}`}
                id={`rbt-menu-item-${position}`}
                role='option'
            >
                <a
                    className={`dropdown-item ${disabled ? 'disabled' : ''}`}
                    href=''
                    onClick={(e) => {
                        e.preventDefault()
                        !disabled && onAsyncPaginate(text, asyncPaginate.page + 1);
                    }}
                >
                    {label}
                </a>
            </li>
        </Fragment>
    )

class CustomTypeaheadMenu extends Component {

    renderCustomMenuItem = (option, index) => {
        const {
            asyncPaginate,
            labelKey,
            newSelectionPrefix,
            onAsyncPaginate,
            renderMenuItemChildren,
            text,
        } = this.props

        const label = getOptionLabel(option, labelKey)

        const menuItemProps = {
            disabled: option.disabled,
            key: index,
            label,
            option,
            position: index,
        }

        if (option.customOption) {
            return (
                <MenuItem
                    {...menuItemProps}
                    className='rbt-menu-custom-option'
                    label={newSelectionPrefix + label}
                >
                    {newSelectionPrefix}
                    <Highlighter search={text}>
                        {label}
                    </Highlighter>
                </MenuItem>
            )
        }

        if (option.asyncPaginationOption)
            return (
                <MenuAsyncPaginationItem
                    {...menuItemProps}
                    asyncPaginate={asyncPaginate}
                    onAsyncPaginate={onAsyncPaginate}
                    text={text}
                />
            )

        return (
            <MenuItem {...menuItemProps}>
                {renderMenuItemChildren(option, this.props, index)}
            </MenuItem>
        )
    }

    render() {
        const {
            asyncPaginate,
            isLoading,
            onAsyncPaginate,
            options,
            paginationText,
            ...menuProps
        } = this.props

        let _options = options
        const customOption = options.length && !(options.length === 1 && options.findIndex((opt) => opt.customOption) !== -1)

        if (customOption && asyncPaginate && asyncPaginate.page !== asyncPaginate.lastPage)
            _options = options.concat([{
                asyncPaginationOption: true,
                disabled: isLoading,
                [getStringLabelKey(menuProps.labelKey)]: isLoading ? 'Cargando resultados...' : paginationText
            }])

        return (
            <Menu {...menuProps}>
                {_options.map(this.renderCustomMenuItem)}
            </Menu>
        )
    }
}

CustomTypeaheadMenu.propTypes = {
    asyncPaginate: PropTypes.shape({ // Custom prop
        lastPage: PropTypes.number,
        page: PropTypes.number,
        total: PropTypes.number,
    }),
    isLoading: PropTypes.bool,
    newSelectionPrefix: proptypes.newSelectionPrefix,
    onAsyncPaginate: PropTypes.func, //Custom prop
    options: PropTypes.array.isRequired,
    paginationText: proptypes.paginationText,
    renderMenuItemChildren: proptypes.renderMenuItemChildren
}

CustomTypeaheadMenu.defaultProps = {
    isLoading: false,
    paginationText: defaultProps.paginationText,
    renderMenuItemChildren: (option, props, index) => (
        <Highlighter search={props.text}>
            {getOptionLabel(option, props.labelKey)}
        </Highlighter>
    )
}

export default CustomTypeaheadMenu