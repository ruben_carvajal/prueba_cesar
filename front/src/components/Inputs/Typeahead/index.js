import AsyncTypeahead from './AsyncTypeahead'
import Typeahead from './Typeahead'

export default Typeahead

export {
    AsyncTypeahead
}