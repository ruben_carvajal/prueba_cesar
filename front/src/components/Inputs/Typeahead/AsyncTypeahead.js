import React from 'react'
import PropTypes from 'prop-types'
import { AsyncTypeahead } from 'react-bootstrap-typeahead'

import CommonInput from '../CommonInput'
import CustomTypeaheadMenu from './TypeaheadMenu'
import { parseFieldProps } from '../utils'

import { defaultProps } from './defaults'
import proptypes from './proptypes'

const CustomAsyncTypeahead = ({
    asyncPaginate,
    containerClassName,
    delay,
    isInvalid,
    isLoading,
    label,
    onAsyncPaginate,
    onBlur,
    onChange,
    onSearch,
    options,
    paginationText,
    promptText,
    renderMenu,
    required,
    searchText,
    selected,
    ...props
}) => {
    const { error, onBlur: inputBlur, onChange: inputChange, value: inputValue } = parseFieldProps(props)

    return (
        <CommonInput
            className={containerClassName}
            label={label}
            required={required}
            {...props}
        >
            <AsyncTypeahead
                delay={delay}
                isInvalid={error || isInvalid}
                isLoading={isLoading}
                onBlur={inputBlur ? () => inputBlur(inputValue) : onBlur}
                onChange={inputChange || onChange}
                onSearch={onSearch}
                options={options}
                promptText={promptText}
                renderMenu={(results, menuProps) => (
                    <CustomTypeaheadMenu
                        asyncPaginate={asyncPaginate}
                        isLoading={isLoading}
                        onAsyncPaginate={onAsyncPaginate}
                        options={results}
                        paginationText={paginationText}
                        {...menuProps}
                    />
                )}
                searchText={searchText}
                selected={inputValue || selected}
                {...props}
            />
        </CommonInput>
    )
}

CustomAsyncTypeahead.propTypes = {
    ...proptypes,
    asyncPaginate: PropTypes.shape({ // Custom prop
        lastPage: PropTypes.number,
        page: PropTypes.number,
        total: PropTypes.number,
    }),
    delay: PropTypes.number,
    isLoading: PropTypes.bool,
    onAsyncPaginate: PropTypes.func,
    onSearch: PropTypes.func.isRequired,
    options: PropTypes.array,
    promptText: PropTypes.node,
    searchText: PropTypes.node,
    useCache: PropTypes.bool,
}

CustomAsyncTypeahead.defaultProps = {
    ...defaultProps,
    delay: 300,//default 200
    isLoading: false,
    options: [],
    promptText: 'Ingrese búsqueda...',
    searchText: 'Buscando...',
    useCache: false //default true
}

export default CustomAsyncTypeahead