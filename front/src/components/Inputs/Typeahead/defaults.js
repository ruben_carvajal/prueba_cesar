export const defaultLabelKey = 'label' //default label

export const defaultProps = {
    align: 'justify', // default 'justify'
    allowNew: false, //default false
    autoFocus: false, //default false
    bodyContainer: false, //default false
    caseSensitive: false, //default false
    clearButton: true, //default false
    defaultInputValue: '', //default ''
    defaultSelected: [], //default []
    dropup: false, //default false
    emptyLabel: 'No hay coincidencias',
    filterBy: [], //default []
    flip: false, //default false
    highlightOnlyResult: false, //default false
    ignoreDiacritics: true, //default true
    inputProps: {}, //default {}
    isInvalid: false, //default false
    isLoading: false, //default false
    isValid: false, //default false
    labelKey: defaultLabelKey,
    maxHeight: '300px', //default 300px
    maxResults: 100, //default 100 for performance reasons so as not to render too many DOM nodes in the case of large data sets.
    minLength: 3, //default 0
    multiple: false, //default false
    newSelectionPrefix: 'Nueva selección: ',
    options: [],
    paginate: false, //default true
    paginationText: 'Mostrar más resultados...',
    selected: [], //default []
    selectHintOnEnter: true,//default false
}