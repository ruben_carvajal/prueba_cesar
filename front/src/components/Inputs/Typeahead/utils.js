import { defaultLabelKey } from './defaults'

export const getStringLabelKey = (labelKey) => (
    typeof labelKey === 'string' ? labelKey : defaultLabelKey
)

export const getOptionLabel = (option, labelKey) => {
    if (option.paginationOption || option.asyncPaginationOption || option.customOption)
        return option[getStringLabelKey(labelKey)]

    let optionLabel
    if (typeof option === 'string')
        optionLabel = option
    if (typeof labelKey === 'function')
        optionLabel = labelKey(option);
    else if (typeof labelKey === 'string')
        optionLabel = option[labelKey]
    return optionLabel
}