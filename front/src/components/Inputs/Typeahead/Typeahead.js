import React from 'react'
import { Typeahead } from 'react-bootstrap-typeahead'

import CommonInput from '../CommonInput'
import { parseFieldProps } from '../utils'

import { defaultProps } from './defaults'
import proptypes from './proptypes'

const CustomTypeahead = ({
    a11yNumResults,
    a11yNumSelected,
    align,
    allowNew,
    autoFocus,
    bodyContainer,
    bsSize,
    caseSensitive,
    clearButton,
    containerClassName,
    defaultInputValue,
    defaultSelected,
    disabled,
    dropup,
    emptyLabel,
    filterBy,
    flip,
    highlightOnlyResult,
    ignoreDiacritics,
    inputProps,
    isInvalid,
    isLoading,
    isValid,
    label,
    labelKey,
    maxHeight,
    maxResults,
    minLength,
    multiple,
    newSelectionPrefix,
    onBlur,
    onChange,
    onFocus,
    onInputChange,
    onKeyDown,
    onMenuHide,
    onMenuShow,
    onPaginate,
    options,
    paginate,
    paginationText,
    placeholder,
    renderMenu,
    renderMenuItemChildren,
    renderToken,
    required,
    selected,
    selectHintOnEnter,
    ...props
}) => {
    const { error, onBlur: inputBlur, onChange: inputChange, value: inputValue } = parseFieldProps(props)

    return (
        <CommonInput
            className={containerClassName}
            label={label}
            required={required}
            {...props}
        >
            <Typeahead
                a11yNumResults={a11yNumResults}
                a11yNumSelected={a11yNumSelected}
                align={align}
                allowNew={allowNew}
                autoFocus={autoFocus}
                bodyContainer={bodyContainer}
                bsSize={bsSize}
                caseSensitive={caseSensitive}
                clearButton={clearButton}
                defaultInputValue={defaultInputValue}
                defaultSelected={defaultSelected}
                disabled={disabled}
                dropup={dropup}
                emptyLabel={emptyLabel}
                filterBy={filterBy}
                flip={flip}
                highlightOnlyResult={highlightOnlyResult}
                ignoreDiacritics={ignoreDiacritics}
                inputProps={inputProps}
                isInvalid={error || isInvalid}
                isLoading={isLoading}
                isValid={isValid}
                labelKey={labelKey}
                maxHeight={maxHeight}
                maxResults={maxResults}
                minLength={minLength}
                multiple={multiple}
                newSelectionPrefix={newSelectionPrefix}
                onBlur={inputBlur ? () => inputBlur(inputValue) : onBlur}
                onChange={inputChange || onChange}
                onFocus={onFocus}
                onInputChange={onInputChange}
                onKeyDown={onKeyDown}
                onMenuHide={onMenuHide}
                onMenuShow={onMenuShow}
                onPaginate={onPaginate}
                options={options}
                paginate={paginate}
                paginationText={paginationText}
                placeholder={placeholder}
                renderMenu={renderMenu}
                renderMenuItemChildren={renderMenuItemChildren}
                renderToken={renderToken}
                selected={inputValue || selected}
                selectHintOnEnter={selectHintOnEnter}
            />
        </CommonInput>
    )
}

CustomTypeahead.propTypes = proptypes

CustomTypeahead.defaultProps = defaultProps

export default CustomTypeahead