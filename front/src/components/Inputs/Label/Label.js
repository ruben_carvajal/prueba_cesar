import React from 'react'
import PropTypes from 'prop-types'
import { Label } from 'reactstrap'
import RequiredLabel from './RequiredLabel'
import { inputProptypes } from '../proptypes'

const CustomLabel = ({
    className,
    label,
    required,
}) => {
    return (
        <Label
            className={className}>
            {label}
            {required && <RequiredLabel />}:
        </Label>
    )
}

CustomLabel.propTypes = {
    className: PropTypes.string,
    label: inputProptypes.label,
    required: inputProptypes.required
}

CustomLabel.defaultProps = {
    className: ''
}

export default CustomLabel