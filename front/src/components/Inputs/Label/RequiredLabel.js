import React from 'react'

const RequiredLabel = () => (
    <span className='text-danger'>
        <sup>*</sup>
    </span>
)

export default RequiredLabel