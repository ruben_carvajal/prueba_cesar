import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import {
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText
} from 'reactstrap'

import CommonInput from '../CommonInput'
import { parseFieldProps } from '../utils'

const CustomInputGroupAddon = ({
    addon,
    addonType,
    icon
}) => (
        <InputGroupAddon addonType={addonType}>
            <InputGroupText>
                {addon ?
                    addon :
                    icon && <i className={icon}></i>
                }
            </InputGroupText>
        </InputGroupAddon>
    )

const CustomInputGroup = ({
    addon,
    addonType,
    className,
    containerClassName,
    icon,
    inputClassName,
    label,
    required,
    ...props
}) => {
    const { error, invalid, touched, ...inputProps } = parseFieldProps(props)
    return (
        <CommonInput
            className={containerClassName}
            label={label}
            required={required}
            {...props}
        >
            <InputGroup
                className={className}
            >
                {addonType === 'prepend' ?
                    (
                        <Fragment>
                            <CustomInputGroupAddon
                                addon={addon}
                                addonType={addonType}
                                icon={icon}
                            />
                            <Input
                                className={inputClassName}
                                invalid={error}
                                {...inputProps}
                                {...props}
                            />
                        </Fragment>
                    ) : (
                        <Fragment>
                            <Input
                                className={inputClassName}
                                invalid={error}
                                {...inputProps}
                                {...props}
                            />
                            <CustomInputGroupAddon
                                addon={addon}
                                addonType={addonType}
                                icon={icon}
                            />
                        </Fragment>
                    )
                }
            </InputGroup>
        </CommonInput>
    )
}
CustomInputGroup.propTypes = {
    addon: PropTypes.any,
    addonType: PropTypes.oneOf(['append', 'prepend']),
    className: PropTypes.string,
    containerClassName: PropTypes.string,
    icon: PropTypes.string,
    inputClassName: PropTypes.string,
}

CustomInputGroup.defaultProps = {
    addonType: 'prepend',
}

export default CustomInputGroup