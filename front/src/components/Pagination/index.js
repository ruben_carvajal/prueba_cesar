import CustomPagination from './Pagination'
import SimplePagination from './SimplePagination'

export default CustomPagination

export {
    SimplePagination
}