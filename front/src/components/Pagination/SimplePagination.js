import React from 'react'

import Pagination from './Pagination'

const SimplePagination = ({
    activePage,
    perPage,
    total,
    ...paginationProps
}) => {

    const totalPages = Math.ceil(total / perPage);
    return (
        <div className='d-flex flex-column flex-md-row justify-content-between align-items-center'>
            {total > 1 && <div className='text-center mb-2 mb-md-0'>
                {`
                    ${total > 0 ? ((activePage - 1) * perPage + 1) : 0} - 
                    ${ activePage * perPage < total ? (activePage * perPage) : total} 
                    de ${total} elementos
                `}
            </div>}
            {totalPages > 1 &&
                <Pagination
                    {...paginationProps}
                    activePage={activePage}
                    items={totalPages}
                />}
        </div>
    )
}

export default SimplePagination