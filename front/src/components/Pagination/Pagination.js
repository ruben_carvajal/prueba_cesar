import React from 'react'
import PropTypes from 'prop-types'
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap'

const PaginationEllipsis = (props) => (
    <PaginationItem
        disabled
        {...props}
    >
        <PaginationLink
            href='#'
        >
            ...
        </PaginationLink>
    </PaginationItem>
)

const PageItems = ({
    activePage,
    boundaries,
    ellipsis,
    items,
    maxItems,
    onSelect
}) => {
    const pageItems = []
    let endPage, startPage

    if (maxItems >= items) {
        startPage = 1
        endPage = items
    } else {
        startPage = Math.max(
            Math.min(
                activePage - Math.floor(maxItems / 2),
                items - maxItems + 1
            ),
            1
        )
        endPage = startPage + maxItems - 1
    }

    if (boundaries && startPage > 1) {
        pageItems.push(
            <PaginationItem
                key='pagination-item-1'
            >
                <PaginationLink
                    href='#'
                    onClick={() => onSelect(1)}
                >
                    1
                </PaginationLink>
            </PaginationItem>
        )
    }

    if (ellipsis && startPage > 1) {
        pageItems.push(
            <PaginationEllipsis
                key='pagination-first-ellipsis'
            />
        )
    }

    for (let page = startPage; page <= endPage; ++page) {
        pageItems.push(
            <PaginationItem
                active={page === activePage}
                key={`pagination-item-${page}`}
            >
                <PaginationLink
                    href='#'
                    onClick={() => page !== activePage ? onSelect(page) : null}
                >
                    {page}
                </PaginationLink>
            </PaginationItem>
        );
    }


    if (ellipsis && endPage < items) {
        pageItems.push(
            <PaginationEllipsis
                key='pagination-last-ellipsis'
            />
        )
    }

    if (boundaries && endPage < items) {
        pageItems.push(
            <PaginationItem
                key={`pagination-item-${items}`}
            >
                <PaginationLink
                    href='#'
                    onClick={() => onSelect(items)}
                >
                    {items}
                </PaginationLink>
            </PaginationItem>
        )
    }

    return pageItems
}

const CustomPagination = ({
    activePage,
    boundaries,
    ellipsis,
    items,
    maxItems,
    next,
    onSelect,
    previous,
    size
}) => {
    return (
        <Pagination size={size}>
            {previous &&
                <PaginationItem
                    disabled={activePage === 1}
                >
                    <PaginationLink
                        href="#"
                        onClick={() => onSelect(activePage - 1)}
                        previous
                    />
                </PaginationItem>}
            <PageItems
                activePage={activePage}
                boundaries={boundaries}
                ellipsis={ellipsis}
                items={items}
                maxItems={maxItems}
                onSelect={onSelect}
            />
            {next &&
                <PaginationItem
                    disabled={activePage >= items}
                >
                    <PaginationLink
                        href="#"
                        next
                        onClick={() => onSelect(activePage + 1)}
                    />
                </PaginationItem>}
        </Pagination>
    )
}

CustomPagination.propTypes = {
    activePage: PropTypes.number,
    boundaries: PropTypes.bool,
    ellipsis: PropTypes.bool,
    items: PropTypes.number,
    maxItems: PropTypes.number,
    next: PropTypes.bool,
    onSelect: PropTypes.func.isRequired,
    previous: PropTypes.bool,
    size: PropTypes.oneOf(['lg', 'sm']),
}

CustomPagination.defaultProps = {
    activePage: 1,
    boundaries: true,
    ellipsis: true,
    items: 1,
    maxItems: 5,
    next: true,
    previous: true,
}

export default CustomPagination