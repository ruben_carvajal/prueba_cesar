import React from 'react'
import PropTypes from 'prop-types'
import BootstrapTable from 'react-bootstrap-table-next'

const CustomTable = ({
    bordered,
    bootstrap4,
    classes,
    columns,
    condensed,
    data,
    headerClasses,
    hover,
    keyField,
    noDataIndication,
    onTableChange,
    remote,
    striped,
    wrapperClasses,
}) => (
        <BootstrapTable
            bordered={bordered}
            bootstrap4={bootstrap4}
            classes={classes}
            columns={columns}
            condensed={condensed}
            data={data}
            headerClasses={headerClasses}
            hover={hover}
            keyField={keyField}
            noDataIndication={noDataIndication}
            onTableChange={onTableChange}
            remote={remote}
            striped={striped}
            wrapperClasses={wrapperClasses}
        />
    )

CustomTable.propTypes = {
    bordered: PropTypes.bool,
    bootstrap4: PropTypes.bool,
    classes: PropTypes.string,
    columns: PropTypes.arrayOf(PropTypes.object).isRequired,
    condensed: PropTypes.bool,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    headerClasses: PropTypes.string,
    hover: PropTypes.bool,
    keyField: PropTypes.string.isRequired,
    noDataIndication: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.string,
    ]),
    onTableChange: PropTypes.func,
    remote: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.object,
    ]),
    striped: PropTypes.bool,
    wrapperClasses: PropTypes.string,
}

CustomTable.defaultProps = {
    bootstrap4: true,
    bordered: false,
    classes: 'table-sm table-borderless',
    condensed: true,
    headerClasses: 'thead-light',
    hover: true,
    noDataIndication: () => `No hay datos para mostrar`,
    striped: true,
    wrapperClasses: 'table-responsive'
}

export default CustomTable