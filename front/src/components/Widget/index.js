import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import {
    UncontrolledTooltip
} from 'reactstrap'
import { Link } from 'react-router-dom'

const Widget = ({
    color,
    error,
    icon,
    id,
    isLoading,
    label,
    linkTarget,
    value
}) => (
        <div
            className={`bg-${color}`}
            style={{
                borderRadius: 6,
                overflow: 'hidden'
            }}
        >
            <div className='d-flex justify-content-between align-items-center p-3'>
                <div
                    className='position-absolute display-3'
                    style={{ color: 'rgba(255,255,255,0.3)' }}
                >
                    <i className={icon}></i>
                </div>
                <div className='w-100 text-right'>
                    <div
                        className='h1 m-0'
                        style={{ fontWeight: 500 }}
                    >
                        {error || isLoading ? '-' : value}
                    </div>
                    <div className='font-weight-bold'>
                        {error &&
                            <Fragment>
                                <i
                                    className={`fas fa-exclamation-circle fa-fw mr-2 text-${color === 'danger' ? 'white' : 'danger'}`}
                                    id={`widget-error-${id}`}
                                />
                                <UncontrolledTooltip
                                    delay={0}
                                    placement='top'
                                    target={`widget-error-${id}`}
                                >
                                    Ha ocurrido un error
                                </UncontrolledTooltip>
                            </Fragment>
                        }
                        {label}
                    </div>
                </div>
            </div>
            {linkTarget && <div
                className='py-1 text-center'
                style={{
                    backgroundColor: 'rgba(0,0,0,0.4)',
                }}
            >
                {isLoading ?
                    <span className={`text-${color}`}>Cargando espere...</span> :
                    (error ?
                        <span className='text-white'>
                            Ha ocurrido un error<i className='fas fa-exclamation-circle fa-fw ml-2 text-danger'></i>
                        </span> :
                        <Link
                            className={`text-${color}`}
                            to={linkTarget}
                        >
                            Ver detalle<i className='fas fa-arrow-right fa-fw ml-2'></i>
                        </Link>)
                }
            </div>}
        </div>
    )

Widget.propTypes = {
    color: PropTypes.oneOf([
        'primary',
        'secondary',
        'success',
        'danger',
        'warning',
        'info',
        'light'
    ]),
    error: PropTypes.bool,
    icon: PropTypes.string,
    id: PropTypes.string.isRequired,
    isLoading: PropTypes.bool,
    label: PropTypes.node,
    linkTarget: PropTypes.oneOfType([
        PropTypes.object,
        PropTypes.string,
    ]),
    value: PropTypes.node,
}

Widget.defaultProps = {
    color: 'primary',
    error: false,
    isLoading: false,
}

export default Widget