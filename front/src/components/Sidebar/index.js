import React, { Fragment } from 'react'
import { withRouter } from 'react-router-dom'
import {
    AppSidebarNav,
    AppSidebarFooter
} from '@coreui/react'

import {
    nav as navConfig
} from '../../config'

const Sidebar = (props) => (
    <Fragment>
        <AppSidebarNav
            navConfig={{ items: navConfig }}
            {...props}
        />
        <AppSidebarFooter className='text-center small'>
            &copy; Santa Priscila
        </AppSidebarFooter>
    </Fragment>
)

export default withRouter(Sidebar)