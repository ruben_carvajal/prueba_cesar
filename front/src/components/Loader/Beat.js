import React from 'react'
import {
    BeatLoader
} from 'halogenium'

import { defaultProps } from './defaults'
import proptypes from './proptypes'

const Beat = (props) => (
    <BeatLoader
        {...props}
    />
)

Beat.propTypes = proptypes

Beat.defaultProps = defaultProps

export default Beat