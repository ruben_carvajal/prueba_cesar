/* 
    Loaders default props  
*/
export const defaultProps = {
    color: 'var(--primary)',
    loading: true,
    size: 15
}