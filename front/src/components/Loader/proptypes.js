import PropTypes from 'prop-types'

/* 
    Loaders proptypes 
*/
export default {
    color: PropTypes.string,
    loading: PropTypes.bool,
    size: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ]),
}