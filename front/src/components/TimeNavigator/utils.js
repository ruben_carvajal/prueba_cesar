import moment from 'moment'

export const calculateRange = (date, timeUnit) => ({
    date,
    endDate: moment(date).endOf(timeUnit).toDate(),
    startDate: moment(date).startOf(timeUnit).toDate(),
    timeUnit,
})