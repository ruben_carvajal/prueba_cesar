import { calculateRange } from './utils'

export const DEFAULT_VIEW = 'month'
export const TODAY = new Date()
export const DEFAULT_RANGE = calculateRange(TODAY, DEFAULT_VIEW)
export const VIEWS = [
    {
        key: 'week',
        name: 'Semana'
    },
    {
        key: 'month',
        name: 'Mes'
    },
    {
        key: 'year',
        name: 'Año'
    }
]