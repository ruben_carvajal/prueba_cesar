import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
    Button,
    ButtonGroup,
    ButtonToolbar,
} from 'reactstrap'
import moment from 'moment'

import { DEFAULT_RANGE, DEFAULT_VIEW, TODAY, VIEWS } from './defaults'
import { calculateRange } from './utils'

class TimeNavigator extends Component {

    onChangeTimeNav = (actualDate, viewMode, dateOffset, callback) =>
        callback(calculateRange(moment(actualDate).add(dateOffset, viewMode).toDate(), viewMode))

    onChangeTimeView = (actualDate, viewMode, callback) =>
        callback(calculateRange(actualDate, viewMode))

    render() {
        const {
            date,
            disabled,
            onChange,
            timeUnit: activeView
        } = this.props

        const isToday = moment(date).isSame(TODAY, activeView)

        return (
            <ButtonToolbar className='justify-content-center'>
                <ButtonGroup className='mx-2 my-2 my-sm-0'>
                    <Button
                        disabled={disabled}
                        onClick={() => this.onChangeTimeNav(date, activeView, -1, onChange)}
                        outline
                    >
                        <i className='fas fa-angle-double-left fa-fw' />
                    </Button>
                    <Button
                        disabled={disabled || isToday}
                        onClick={() => this.onChangeTimeNav(TODAY, activeView, 0, onChange)}
                        outline
                    >
                        Hoy
                    </Button>
                    <Button
                        disabled={disabled || isToday}
                        onClick={() => this.onChangeTimeNav(date, activeView, 1, onChange)}
                        outline
                    >
                        <i className='fas fa-angle-double-right fa-fw' />
                    </Button>
                </ButtonGroup>
                <ButtonGroup className='mx-2 my-2 my-sm-0'>
                    {VIEWS.map((view, index) =>
                        <Button
                            active={activeView === view.key}
                            disabled={disabled}
                            key={`time-navigator-${index}`}
                            onClick={() => this.onChangeTimeView(date, view.key, onChange)}
                            outline
                        >
                            {view.name}
                        </Button>
                    )}
                </ButtonGroup>
            </ButtonToolbar>
        )
    }
}

TimeNavigator.propTypes = {
    date: PropTypes.instanceOf(Date).isRequired,
    disabled: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
    timeUnit: PropTypes.oneOf([
        'month',
        'week',
        'year'
    ]).isRequired,
}

TimeNavigator.defaultProps = {
    date: TODAY,
    disabled: false,
    timeUnit: DEFAULT_VIEW
}

export default Object.assign(TimeNavigator, { DEFAULT_RANGE })