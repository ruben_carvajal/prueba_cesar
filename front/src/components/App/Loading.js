import React from 'react'

import Alert from '../Alert'

const Retry = ({
    color,
    onRetry
}) => (
        <a
            className={`font-weight-bold text-${color} ml-2`}
            href=''
            onClick={(e) => {
                e.preventDefault()
                onRetry && onRetry()
            }}
        >
            <i className='fas fa-redo-alt fa-fw mr-1' />
            Volver a intentar
        </a>
    )

const Loading = ({
    error,
    pastDelay,
    retry,
    timedOut
}) => {

    if (error)
        return (
            <Alert
                message={
                    <div>
                        Ha ocurrido un error
                        <Retry
                            color='danger'
                            onRetry={retry}
                        />
                    </div>
                }
            />
        )

    if (timedOut)
        return (
            <Alert
                color='warning'
                message={
                    <div>
                        Al parecer está demorando mucho...
                        <Retry
                            color='warning'
                            onRetry={retry}
                        />
                    </div>
                }
            />
        )

    if (pastDelay)
        return (
            <Alert
                color='info'
                message='Cargando espere...'
                title='Un momento'
            />
        )

    return null

}

export default Loading