import React from 'react'
import { Container } from 'reactstrap'
import {
    AppHeader,
    AppSidebar
} from '@coreui/react'

import Header from '../Header'
import Sidebar from '../Sidebar'

const App = ({ children }) => (
    <div className='app'>
        <AppHeader className='navbar-light' fixed>
            <Header />
        </AppHeader>
        <div className='app-body'>
            <AppSidebar display='lg' fixed>
                <Sidebar />
            </AppSidebar>
            <main className='main mt-lg-4'>
                <Container fluid>
                    {children}
                </Container>
            </main>
        </div>
    </div>
)

export default App