import TitleWrapper from './TitleWrapper'
import withTitle from './withTitle'

export default TitleWrapper

export { withTitle }