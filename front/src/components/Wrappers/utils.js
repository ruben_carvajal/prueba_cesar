
import { app as appConfig } from '../../config'

export const setDocumentTitle = ({ title }) =>
    document.title = `${title} | ${appConfig.name} `