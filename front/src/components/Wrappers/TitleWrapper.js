import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Route } from 'react-router-dom'

import { setDocumentTitle } from './utils'

class TitleWrapper extends Component {

    componentDidMount() {
        setDocumentTitle(this.props)
    }

    componentDidUpdate(prevProps) {
        if (this.props.title !== prevProps.title)
            setDocumentTitle(this.props)
    }

    render() {
        const { title, ...rest } = this.props
        return (
            <Route
                {...rest}
            />
        )
    }
}

TitleWrapper.propTypes = {
    title: PropTypes.string.isRequired
}

export default TitleWrapper