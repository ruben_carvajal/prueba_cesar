
import React, { Component } from 'react'

import { setDocumentTitle } from './utils'

export default (ComponentWrapper, title) =>

    class WrappedComponent extends Component {

        componentDidMount() {
            setDocumentTitle({ title })
        }

        render() {
            return (
                <ComponentWrapper
                    {...this.props}
                />
            )
        }

    }