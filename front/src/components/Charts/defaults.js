/* 
    Common Charts default props  
*/
export const defaultProps = {
    legend: false,
    tooltip: true,
    xAxis: true,
    yAxis: true
}

export const cartesianGridProps = {
    strokeDasharray: '5 5',
    vertical: false
}

export const xLabelProps = {
    className: 'font-weight-bold',
    position: 'bottom',
}

export const yLabelProps = {
    angle: -90,
    className: 'font-weight-bold',
    position: 'insideLeft',
}