import React from 'react'
import PropTypes from 'prop-types'
import {
    Cell,
    Pie,
    PieChart,
} from 'recharts'

import ChartContainer from './Container'
import proptypes from './proptypes'

const sliceColors = [
    'var(--blue)',
    'var(--cyan)',
    'var(--yellow)',
    'var(--orange)',
]

const CustomPieChart = ({
    data,
    dataKey,
    nameKey,
    title,
}) => (
        <ChartContainer
            title={title}
        >
            <PieChart>
                <Pie
                    data={data}
                    dataKey={dataKey}
                    nameKey={nameKey}
                >
                    {data.map((entry, index) => (
                        <Cell
                            key={`cell-piechart-${index}`}
                            fill={sliceColors[index % sliceColors.length]}
                        />
                    ))}
                </Pie>
            </PieChart>
        </ChartContainer>
    )

CustomPieChart.propTypes = {
    data: proptypes.data,
    dataKey: PropTypes.string,
    nameKey: PropTypes.string,
}

export default CustomPieChart