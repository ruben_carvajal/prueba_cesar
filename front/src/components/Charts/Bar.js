import React from 'react'
import PropTypes from 'prop-types'
import {
    Bar,
    BarChart,
    CartesianGrid,
    Legend,
    Tooltip,
    XAxis,
    YAxis,
} from 'recharts'

import ChartContainer from './Container'
import proptypes from './proptypes'
import {
    cartesianGridProps,
    defaultProps,
    xLabelProps,
    yLabelProps
} from './defaults'

const CustomBarChart = ({
    bars,
    barGap,
    barSize,
    data,
    legend,
    title,
    tooltip,
    tooltipValueFormatter,
    xAxis,
    xDataKey,
    xLabel,
    xTickFormatter,
    yAxis,
    yLabel,
    yTickFormatter
}) => (
        <ChartContainer
            title={title}
        >
            <BarChart
                barSize={barSize}
                barGap={barGap}
                data={data}
            >
                {bars.map((bar, index) => (
                    <Bar
                        dataKey={bar.dataKey}
                        fill={bar.fill}
                        key={`barchart-${index}`}
                        name={bar.name}
                        stackId={bar.stackId}
                    />
                ))}
                <CartesianGrid  {...cartesianGridProps} />
                {legend && <Legend />}
                {tooltip && <Tooltip
                    formatter={tooltipValueFormatter}
                />}
                {xAxis && <XAxis
                    dataKey={xDataKey}
                    label={{
                        ...xLabelProps,
                        value: xLabel,
                    }}
                    tickFormatter={xTickFormatter}
                />}
                {yAxis && <YAxis
                    label={{
                        ...yLabelProps,
                        value: yLabel
                    }}
                    tickFormatter={yTickFormatter}
                />}
            </BarChart>
        </ChartContainer>
    )

CustomBarChart.propTypes = {
    ...proptypes,
    bars: PropTypes.arrayOf(PropTypes.shape({
        dataKey: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string,
        ]),
        fill: PropTypes.string,
        name: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string,
        ]),
        stackId: PropTypes.oneOfType([
            PropTypes.number,
            PropTypes.string,
        ]),
    })).isRequired,
    barGap: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    barSize: PropTypes.number,
}

CustomBarChart.defaultProps = {
    ...defaultProps,
    bars: [],
    barGap: '40%',
    barSize: 30,
}

export default CustomBarChart