import PropTypes from 'prop-types'

export default {
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    legend: PropTypes.bool,
    tooltip: PropTypes.bool,
    tooltipValueFormatter: PropTypes.func,
    xAxis: PropTypes.bool,
    xDataKey: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    xLabel: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    xTickFormatter: PropTypes.func,
    yAxis: PropTypes.bool,
    yLabel: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    yTickFormatter: PropTypes.func,
}