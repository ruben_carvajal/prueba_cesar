import React from 'react'
import PropTypes from 'prop-types'
import { ResponsiveContainer } from 'recharts'

const CustomChartContainer = ({
    aspect,
    children,
    height,
    minWidth,
    title
}) => (
        <div
            style={{
                overflowX: 'auto',
                overflowY: 'hidden'
            }}
        >
            {title &&
                <h5 className='text-center'>
                    {title}
                </h5>}
            <ResponsiveContainer
                aspect={aspect}
                height={height}
                minWidth={minWidth}
            >
                {children}
            </ResponsiveContainer>
        </div>
    )

CustomChartContainer.propTypes = {
    aspect: PropTypes.number,
    children: PropTypes.element.isRequired,
    height: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    minWidth: PropTypes.number,
    title: PropTypes.string,
}

CustomChartContainer.defaultProps = {
    height: 400,
    minWidth: 500,
}

export default CustomChartContainer