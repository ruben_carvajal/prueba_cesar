import React from 'react'
import PropTypes from 'prop-types'
import {
    Area,
    AreaChart,
    CartesianGrid,
    Legend,
    Tooltip,
    XAxis,
    YAxis
} from 'recharts'

import ChartContainer from './Container'
import proptypes from './proptypes'
import {
    cartesianGridProps,
    defaultProps,
    xLabelProps,
    yLabelProps,
} from './defaults'

const CustomAreaChart = ({
    data,
    legend,
    title,
    tooltip,
    tooltipValueFormatter,
    type,
    xAxis,
    xDataKey,
    xLabel,
    xTickFormatter,
    yAxis,
    yDataKey,
    yLabel,
    yName,
    yTickFormatter
}) => (
        <ChartContainer
            title={title}
        >
            <AreaChart
                data={data}
                margin={{
                    bottom: xLabel ? 60 : 40,
                    left: 15,
                    right: 15,
                    top: 15,
                }}
            >
                <CartesianGrid {...cartesianGridProps} />
                {legend && <Legend />}
                {tooltip && <Tooltip
                    formatter={tooltipValueFormatter}
                />}
                {xAxis && <XAxis
                    dataKey={xDataKey}
                    label={{
                        ...xLabelProps,
                        offset: 50,
                        value: xLabel,
                    }}
                    tick={{
                        angle: -45,
                        textAnchor: 'end'
                    }}
                    tickFormatter={xTickFormatter}
                />}
                {yAxis && <YAxis
                    label={{
                        ...yLabelProps,
                        value: yLabel
                    }}
                    tickFormatter={yTickFormatter}
                />}
                <Area
                    dataKey={yDataKey}
                    fill='var(--primary)'
                    name={yName}
                    type={type}
                />
            </AreaChart>
        </ChartContainer>
    )

CustomAreaChart.propTypes = {
    ...proptypes,
    type: PropTypes.oneOf([
        'basis',
        'basisClosed',
        'basisOpen',
        'linear',
        'linearClosed',
        'monotone',
        'monotoneX',
        'monotoneY',
        'natural',
        'step',
        'stepBefore',
        'stepAfter'
    ]),
    yDataKey: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    yName: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
}

CustomAreaChart.defaultProps = defaultProps

export default CustomAreaChart