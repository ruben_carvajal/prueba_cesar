import React from 'react'
import { BrowserRouter, Switch } from 'react-router-dom'
import Authenticated from './components/Session'
import { withTitle } from './components/Wrappers'
import { Login, ForgotPassword } from './pages/Auth'

import ProtectedRoutes from './ProtectedRoutes'

const AppRoutes = () => (
    <BrowserRouter basename="/">
        <Switch>
            {/* Public Routes - without authentication - */}
            <Authenticated path='/login' exact component={withTitle(Login, 'Iniciar Sesión')} />
            <Authenticated path='/forgot' exact component={withTitle(ForgotPassword, 'Olvidó Contraseña')} />
            
            {/* Protected Routes - login required - */}
            <Authenticated
                authenticated
                component={ProtectedRoutes}
                path='/'
            />
        </Switch>
    </BrowserRouter>
)

export default AppRoutes