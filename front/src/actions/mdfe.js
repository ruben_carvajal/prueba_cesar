import axios from 'axios'

const errorMessage = 'There was a problem in our servers, please try again in some minutes'

export const createMDFe = (data) => {
    let route = 'amazon/createMDFe'
    if(process.env.NODE_ENV === 'development')
        route = 'http://localhost:8080/amazon/createMDFe'
    return axios.post(route, data)
        .then(response => {
            return { err: null, mdfe: response }
        }).catch(err => {
            return { err: errorMessage, mdfe: null }
        })
}


export const listMDFe = (data) => {
    let route = 'amazon/listMDFe'
    if(process.env.NODE_ENV === 'development')
        route = 'http://localhost:8080/amazon/listMDFe'
    return axios.post(route, data)
        .then(response => {
            return { err: null, data: response }
        }).catch(err => {
            return { err: errorMessage, mdfe: null }
        })
}

export const downloadDoc = (data) => {
    let route = 'amazon/downloadMDFe'
    if(process.env.NODE_ENV === 'development')
        route = 'http://localhost:8080/amazon/downloadMDFe'
    return axios.post(route, data)
        .then(response => {
            return { err: null, data: response }
        }).catch(err => {
            return { err: errorMessage, mdfe: null }
        })
}

export const autorizarMDFe = (data) => {
    let route = 'amazon/authorizedMDFe'
    if(process.env.NODE_ENV === 'development')
        route = 'http://localhost:8080/amazon/authorizedMDFe'
    return axios.post(route, data)
        .then(response => {
            return { err: null, data: response }
        }).catch(err => {
            return { err: errorMessage, mdfe: null }
        })
}

export const cerrarMDFe = (data) => {
    let route = 'amazon/closeMDFe'
    if(process.env.NODE_ENV === 'development')
        route = 'http://localhost:8080/amazon/closeMDFe'
    return axios.post(route, data)
        .then(response => {
            return { err: null, data: response }
        }).catch(err => {
            return { err: errorMessage, mdfe: null }
        })
}

/**
 * Since here we have functions related to Manifest
 */

export const getManifestListByDate = (data) => {
    let route = 'amazon/manifestlist'
    if(process.env.NODE_ENV === 'development')
        route = 'http://localhost:8080/amazon/manifestlist'
    return axios.post(route, data)
        .then(response => {
            console.log('response >>>>, response>', response)
            if(response.result) {
                response.result.forEach(manifest => {
                    manifest.deliveryTags = manifest.deliveryTags ? JSON.parse(manifest.deliveryTags) : []
                    manifest.included = false
                });
            }
            return { err: null, data: response }
        }).catch(err => {
            return { err: errorMessage, mdfe: null }
        })
}

export const setManifestTag = (data) => {
    let route = 'amazon/setbulktaskamazon'
    if(process.env.NODE_ENV === 'development')
        route = 'http://localhost:8080/amazon/setbulktaskamazon'
    return axios.post(route, data)
        .then(response => {
            return { err: null, data: response }
        }).catch(err => {
            return { err: errorMessage, data: null }
        })
}

export const removeTagAmazon = (data) => {
    let route = 'amazon/removeTagAmazon'
    if(process.env.NODE_ENV === 'development')
        route = 'http://localhost:8080/amazon/removeTagAmazon'
    return axios.post(route, data)
        .then(response => {
            return { err: null, data: response }
        }).catch(err => {
            return { err: errorMessage, data: null }
        })
}

export const getManifestReport = (data) => {
    let route = 'amazon/postManifestReport'
    if(process.env.NODE_ENV === 'development')
        route = 'http://localhost:8080/amazon/postManifestReport'
    return axios.post(route, data)
        .then(response => {
            return { err: null, data: response }
        }).catch(err => {
            return { err: errorMessage, data: null }
        })
}