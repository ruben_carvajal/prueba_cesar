import axios from 'axios'

import {
    SET_AUTHENTICATED,
    SET_AUTH_FEEDBACK,
    SET_AUTH_USER,
} from '../reducers/actions'
import { auth as authConfig } from '../config'

export const setAuthenticated = (authenticated = false, loggedOut = false) => ({
    type: SET_AUTHENTICATED,
    authenticated,
    loggedOut
})

export const setAuthFeedback = (message = null, error = false) => ({
    type: SET_AUTH_FEEDBACK,
    error,
    message,
})

export const setAuthUser = (user = null) => ({
    type: SET_AUTH_USER,
    user,
})

export const authenticate = (credentials) => axios.post('/api/login', credentials)

export const logout = () => axios.post('/api/logout')

export const forgotPassword = (data) => axios.post('/api/forgot', data)

export const recoverPassword = (data) => axios.post('/api/recover', data)

export const userInfo = () => axios.post('/api/me')

export const registerAuthHeaders = () => (dispatch) => {
    axios.interceptors.request.use((config) => {
        try {
            const token = getAuthToken()
            if (token)
                config.headers[authConfig.authHeaderKey] = authConfig.authHeaderValue(token)
            else
                delete config.headers[authConfig.authHeaderKey];
        } catch (error) {
            // Storage cannot be read
        }
        return config;
    }, (error) => Promise.reject(error))

	axios.interceptors.response.use((response) => response, (error) => {
		if (error.response && error.response.status === 401) {
			dispatch(setAuthFeedback(error.message || 'No autorizado', true))
			dispatch(doLogout())
		}
		return Promise.reject(error)
	})
}

export const getAuthToken = () => localStorage.getItem(authConfig.tokenKey)

export const setAuthToken = (token) => localStorage.setItem(authConfig.tokenKey, token)

export const clearLocalStorage = () => localStorage.clear()

export const afterAuthenticated = (userInfo, contribuyenteId) => (dispatch) => (
    new Promise((resolve, reject) => {
        const contribuyentes = userInfo.contribuyentes
        let contribuyenteSelected

        if (contribuyentes && contribuyentes.length === 1)
            contribuyenteSelected = contribuyentes[0]
        else if (contribuyentes && contribuyenteId && !isNaN(contribuyenteId))
            contribuyenteSelected = contribuyentes[
                contribuyentes.findIndex(((contribuyente) => contribuyenteId === contribuyente.id))
            ]

        if (!contribuyenteSelected)
            resolve()
        else {
            // actions after logged in
            console.log('user logged in')
            /*
            dispatch(setContribuyente(contribuyenteSelected))
            getPermisos({ contribuyente: contribuyenteSelected.id }).then(({ permisos }) => {
                getInfoContribuyente({ id: contribuyenteSelected.id }).then(({ empresa }) => {
                    dispatch(setPermisos(permisos))
                    ojearNotificaciones().then(({ notificaciones, sinLeer }) => {
                        dispatch(setNotifications(notificaciones, sinLeer))
                        dispatch(setInfoContribuyente(empresa))
                        resolve()
                    }).catch(error =>
                        reject(error)
                    )
                }).catch(error =>
                    reject(error)
                )
            }).catch((error) =>
                reject(error)
            )*/
        }
    })
)

export const doLogout = () => (dispatch) => {
    dispatch(setAuthenticated(false, true))
    dispatch(setAuthUser())
    // dispatch(setNotifications())
    // dispatch(setPermisos())
    // dispatch(setContribuyente())
    try {
        clearLocalStorage()
    } catch (error) {
        // Error reading storage
    }
} 