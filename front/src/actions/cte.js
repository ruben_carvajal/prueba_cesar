import axios from 'axios'

export const cteGetStatus = (data) => {
    let route = 'amazon/ctestatus'
    if(process.env.NODE_ENV === 'development')
        route = 'http://localhost:8080/amazon/ctestatus'
    return axios.post(route, data)
        .then(response => {
            return { err: null, data: response }
        }).catch(err => {
            return { err: "No se pudo conectar con el servidor. Por favor espere unos minutos e intente nuevamente", mdfe: null }
        })
}


export const cteGenerateReport = (data) => {
    let route = 'amazon/ctereport'
    if(process.env.NODE_ENV === 'development')
        route = 'http://localhost:8080/amazon/ctereport'
    return axios.post(route, data)
        .then(response => {
            return { err: null, data: response }
        }).catch(err => {
            return { err: "No se pudo conectar con el servidor. Por favor espere unos minutos e intente nuevamente", mdfe: null }
        })
}
