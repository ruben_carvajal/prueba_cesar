import React from 'react'
import { Switch } from 'react-router-dom'

import AppLayout from './components/App'
import TitledRoute from './components/Wrappers'

import Home from './pages/Home'
import NotFound from './pages/NotFound'
//import Test from './pages/Test'


const ProtectedRoutes = () => (
    <AppLayout>
        <Switch>
            <TitledRoute component={Home} exact path='/' title='Home' />

            {/* Only for testing */}
            {/*<TitledRoute path='/test' exact component={Test} title='Pruebas' />*/}
            <TitledRoute component={NotFound} title='Página No Encontrada' />
        </Switch>
    </AppLayout>
)

export default ProtectedRoutes