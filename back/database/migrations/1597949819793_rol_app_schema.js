'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RolAppSchema extends Schema {
  up () {
    this.create('rol_app', (table) => {
      table.increments()
      table.timestamps()
      table.integer('rol_id', 10).unsigned().references('id').inTable('rol').notNullable()
      table.integer('app_id', 10).unsigned().references('id').inTable('application').notNullable()

    })
  }

  down () {
    this.drop('rol_app')
  }
}

module.exports = RolAppSchema
