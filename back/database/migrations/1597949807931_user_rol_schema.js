'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UserRolSchema extends Schema {
  up () {
    this.create('user_rol', (table) => {
      table.increments()
      table.timestamps()
      table.integer('user_id', 10).unsigned().references('id').inTable('users').notNullable()
      table.integer('rol_id', 10).unsigned().references('id').inTable('rol').notNullable()
    })
  }

  down () {
    this.drop('user_rol')
  }
}

module.exports = UserRolSchema
