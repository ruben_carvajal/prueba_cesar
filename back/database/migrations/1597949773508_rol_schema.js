'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RolSchema extends Schema {
  up() {
    this.create('rol', (table) => {
      table.increments()
      table.timestamps()
      table.string('nombre', 100)
      table.string('codigo', 100)
    })
  }

  down() {
    this.drop('rol')
  }
}

module.exports = RolSchema
