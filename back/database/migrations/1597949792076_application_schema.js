'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ApplicationSchema extends Schema {
  up () {
    this.create('application', (table) => {
      table.increments()
      table.timestamps()
      table.string('nombre', 100)
      table.string('codigo', 100)
    })
  }

  down () {
    this.drop('application')
  }
}

module.exports = ApplicationSchema
