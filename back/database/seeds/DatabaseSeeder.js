'use strict'

/*
|--------------------------------------------------------------------------
| DatabaseSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const Logger = use('Logger')

class DatabaseSeeder {
  async run () {

     // CReate 2 users
     const User1 = await Factory.model('App/Models/User').create({username:"Cesar1", email:"cesar1@mailinator.com"})
     const User2 = await Factory.model('App/Models/User').create({username:"Cesar2", email:"cesar2@mailinator.com"})

     // create 2 rols
     const Rol1 = await Factory.model('App/Models/Rol').make({nombre:"admin", codigo:"admin"})
     const Rol2 = await Factory.model('App/Models/Rol').make({nombre:"usuario", codigo:"user"})

     // populate many to many tables
     await User1.roles().save(Rol1)
     await User2.roles().save(Rol2)

      // create 2 apps
      const App1 = await Factory.model('App/Models/Application').create({nombre:"Venta", codigo:"venta"})
      const App2 = await Factory.model('App/Models/Application').create({nombre:"Cobro", codigo:"cobro"})

      // populate many to many relation Rol-App
      await Rol1.apps().attach([App1.toJSON().id, App2.toJSON().id])
      await Rol2.apps().attach(App2.toJSON().id)
      //await Rol2.apps().attach(App2)


     Logger.info('Seeding database successfully done!!')

  }
}

module.exports = DatabaseSeeder
