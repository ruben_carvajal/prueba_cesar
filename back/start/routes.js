'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', () => {
  return { greeting: 'Hello world in JSON' }
})

Route.post('api/login', 'UserController.login')

Route.group(() => {
	
	// prueba
	Route.post('prueba/uno', 'PruebaController.uno')
	Route.post('prueba/dos', 'PruebaController.dos')

	// User session
	Route.post('me', 'UserController.userInfo')
	Route.post('logout', 'UserController.userInfo')


}).prefix('api').middleware(['auth:jwt'])