'use strict'

const Logger = use('Logger')
const { validarRequest } = use('App/Services/ValidationRequestService')

class PruebaController {


	async uno({ request, response }) {

		const data = await validarRequest(request, {
			id: { rule: 'required|integer' }
        })
        
        Logger.info('data', data)

        Logger.info('Prueba de logger uno')
        
		return response.send({ message: "message" })
    }
    
    async dos({ request, response }) {

		const data = await validarRequest(request, {
			id: { rule: 'required|integer' }
        })

        Logger.info('data', data)
        
        Logger.error('Prueba de logger error dos')


		return response.send({ message: "message" })
    }
}

module.exports = PruebaController
