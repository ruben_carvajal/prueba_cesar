'use strict'

class UserController {

    async login ({ request, response, auth }) {

        const { email, password } = request.all()

        const loggedUser = await auth.attempt(email, password)
        console.log('loggedUser', loggedUser)

        return response.send({
			userInfo: {},
			token: loggedUser.token
		});
    
    }

    async userInfo({ auth, response }) {
		const { user } = auth;

		const userInfo = user.toJSON();
		return response.send({
			id: userInfo.id,
			username: userInfo.username,
			correo: userInfo.email
		});
    }
    
    async logout({ auth, response }) {

        console.log('auth', auth)
		const { user: { id: usuario_id }, jwtPayload: { data: { suid: sesion_id } } } = auth

		// Se asume que informacion sobre la session existe ya que pasa por el middleware de SesionUsuario
		// await SesionUsuario.query().where('id', sesion_id).where('usuario_id', usuario_id).delete()

		///*** Eliminar la sesión del usuario
		// response.clearCookie('Session-Contribuyente', { path: '/api' })
		return response.send({ message: 'Sesión cerrada exitosamente' })
	}
}

module.exports = UserController
