'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Rol extends Model {

    static get table() {
        return 'rol'
    }


    apps() {
        return this.belongsToMany('App/Models/Application', 'rol_id', 'app_id')
            .pivotTable('rol_app')
    }
}

module.exports = Rol
