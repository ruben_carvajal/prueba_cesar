'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Application extends Model {

    static get table () {
        return 'application'
    }
    
}

module.exports = Application
